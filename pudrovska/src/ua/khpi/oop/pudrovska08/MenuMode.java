package ua.khpi.oop.pudrovska08;

/**
 * Represents the current menu mode.
 */
public enum MenuMode {
	/**
     * Main menu mode (top level of menu is displayed).
     */
    MAIN,
    /**
     * Adding a new element (interface for entering values for the new element's properties).
     */
    ADD_ELEMENT,
    /**
     * Searching through elements (interface for entering a string to search).
     */
    SEARCH_ELEMENTS
}