package ua.khpi.oop.pudrovska08;

import java.time.LocalDate;
import java.util.TreeMap;

/**
 * Represents an audio composition.
 */
public class Composition {
    private final String uniqueId;
    private String title;
    private String album;
    private String artist;
    private final String genre;
    private final String lyrics;
    private final LocalDate dateCreated;
    private final LocalDate datePublished;
    private final long duration;
    private final AudioFormat audioFormat;
    private final TreeMap<String, RatingValue> ratings;
    transient private boolean isLiked;
    
    /**
     * @param uniqueId unique identifier of a composition, can be used in tasks
     * where it is important to uniquely identify a composition,
     * for example, while removing a duplicate composition
     * @param title title of the composition
     * @param album album to which the composition belongs
     * @param artist artist that performs the composition
     * @param genre composition genre
     * @param lyrics composition lyrics (if the composition is not a song or has no lyrics, it is an empty string)
     * @param dateCreated date on which the composition was created
     * @param datePublished date on which the composition was published
     * @param duration duration of the composition, in seconds
     * @param audioFormat format of the composition's audio file
     * @param ratings user ratings of the composition
     */
    public Composition(String uniqueId, String title, String album, String artist,
            String genre, String lyrics, LocalDate dateCreated,
            LocalDate datePublished, long duration, AudioFormat audioFormat,
            TreeMap<String, RatingValue> ratings) {
        this.uniqueId = uniqueId;
        this.title = title;
        this.album = album;
        this.artist = artist;
        this.genre = genre;
        this.lyrics = lyrics;
        this.dateCreated = dateCreated;
        this.datePublished = datePublished;
        this.duration = duration;
        this.audioFormat = audioFormat;
        this.ratings = ratings;
        
        this.isLiked = ratings.values().stream()
                .filter(p -> p.compareTo(RatingValue.SO_SO) > 0)
                .count() >= Math.round((double)ratings.values().stream().count() / 2);
    }

    /**
     * @return unique identifier of a composition
     */
    public String uniqueId() {
        return uniqueId;
    }

    /**
     * @return title of the composition
     */
    public String getTitle() {
        return title;
    }
    
    /** Sets title of the composition.
     * @param title title of the composition
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return album to which the composition belongs
     */
    public String getAlbum() {
        return album;
    }
    
    /** Sets album to which the composition belongs.
     * @param album album to which the composition belongs
     */
    public void setAlbum(String album) {
        this.album = album;
    }

    /**
     * @return artist that performs the composition
     */
    public String getArtist() {
        return artist;
    }
    
    /** Sets artist that performs the composition.
     * @param artist artist that performs the composition
     */
    public void setArtist(String artist) {
        this.artist = artist;
    }

    /**
     * @return composition genre
     */
    public String getGenre() {
        return genre;
    }

    /**
     * @return composition lyrics (if the composition is not a song or has no lyrics, it is an empty string)
     */
    public String getLyrics() {
        return lyrics;
    }

    /**
     * @return date on which the composition was created
     */
    public LocalDate getDateCreated() {
        return dateCreated;
    }

    /**
     * @return date on which the composition was published
     */
    public LocalDate getDatePublished() {
        return datePublished;
    }

    /**
     * @return duration of the composition, in seconds
     */
    public long getDuration() {
        return duration;
    }

    /**
     * @return format of the composition's audio file
     */
    public AudioFormat getDataFormat() {
        return audioFormat;
    }

    /**
     * @return user ratings of the composition
     */
    public TreeMap<String, RatingValue> getRatings() {
        return ratings;
    }

    /**
     * @return boolean value that designates whether composition is liked by users
     */
    public boolean isLiked() {
        return isLiked;
    }

    /**
     * Returns the string representation of the composition.
     * @return string representation of the composition
     */
    @Override
    public String toString() {
        return "Composition [uniqueId=" + uniqueId + ", title=" + title
                + ", album=" + album + ", artist=" + artist + ", genre=" + genre
                + ", lyrics=" + lyrics + ", dateCreated=" + dateCreated
                + ", datePublished=" + datePublished + ", duration=" + duration
                + ", audioFormat=" + audioFormat + ", ratings=" + ratings
                + ", isLiked=" + isLiked + "]";
    }
}
