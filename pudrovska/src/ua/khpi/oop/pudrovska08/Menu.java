package ua.khpi.oop.pudrovska08;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.UUID;
import java.util.stream.Collectors;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

/**
 * Displays program's text menu and saves compositions to file.
 */
public class Menu {
    private MenuMode mode;
    private ArrayList<Composition> compositions;
    
    private static String MSG_INVALID_CHOICE = "Невірний вибір. Будь-ласка, введіть число, що відповідає одному з варіантів.\n";
    
    private int addCount;
    private Composition newComposition;
    
    /**
     * Class constructor.
     * @param compositions compositions the menu will allow a user to work with
     */
    public Menu(ArrayList<Composition> compositions) {
        this.mode = MenuMode.MAIN;
        if (compositions == null) {
            this.compositions = new ArrayList<Composition>();
        } else {
            this.compositions = compositions;
        }
        this.addCount = 0;
    }

    /**
     * Displays the application's text menu.
     */
    public void display() {
        if (this.mode == MenuMode.ADD_ELEMENT) {
            if (addCount < 3) {
                switch (addCount) {
                case 0:
                    // Додамо назву композиції
                    System.out.println("-- Введіть назву нової композиції --");
                    break;
                case 1:
                    // Додамо назву альбому, до якого належить композиція
                    System.out.println("-- Введіть назву альбому, до якого належить нова композиція --");
                    break;
                case 2:
                    // Додамо назву виконавця композиції
                    System.out.println("-- Введіть ім'я виконавця композиції --");
                default:
                    break;
                }
            }
        } else if (this.mode == MenuMode.SEARCH_ELEMENTS) {
            System.out.println("-- Введіть будь-яку частину назви композиції --");
        } else { // Верхній рівень меню
            System.out.println("-- Виберіть дію --");
            System.out.println(
                "  1) Вивести всі композиції на екран\n" +
                "  2) Додати нову композицію\n" +
                "  3) Видалити останню композицію\n" +
                "  4) Шукати композиції за назвою (без урахування регістру)\n" +
                "  5) Відсортувати композиції за назвою\n" +
                "  6) Записати композиції у файл\n" +
                "  7) Вийти з програми\n "
            );
        }
        
        Scanner scanner = new Scanner(System.in);
        
        if (this.mode == MenuMode.ADD_ELEMENT) { // Додавання нової композиції
            if (this.addCount < 3) {
                // Додамо наступну властивість композиції
                if (scanner.hasNextLine()) {
                    String nextString = scanner.nextLine();
                    
                    if (this.newComposition == null) {
                        LocalDate currentLocalDate = Instant.ofEpochMilli(System.currentTimeMillis()).atZone(ZoneId.systemDefault()).toLocalDate();
                        this.newComposition = new Composition(UUID.randomUUID().toString(), "", "", "", "Невідомий",
                                "Текст пісні 1", currentLocalDate, currentLocalDate, 600000, AudioFormat.MP3, new TreeMap<String, RatingValue>());
                    }
                    switch (addCount) {
                    case 0:
                        // Додамо назву композиції
                        this.newComposition.setTitle(nextString);
                        break;
                    case 1:
                        // Додамо назву альбому, до якого входить композиція
                        this.newComposition.setAlbum(nextString);
                        break;
                    case 2:
                        // Додамо назву виконавця композиції
                        this.newComposition.setArtist(nextString);
                    default:
                        break;
                    }
                    this.addCount++;
                }
            } else {
                // Додамо нову композицію до композицій і виведемо щойно додану композицію на екран
                if (this.newComposition != null) {
                    this.compositions.add(this.newComposition);
                    this.mode = MenuMode.MAIN;
                    this.addCount = 0;
                    System.out.println("До аудіотеки додано наступну композицію:\n");
                    this.printComposition(this.compositions.get(this.compositions.size() - 1));
                }
            }
        } else if (this.mode == MenuMode.SEARCH_ELEMENTS) { // Пошук композицій за назвою
            if (scanner.hasNextLine()) {
                String searchString = scanner.nextLine();
                
                ArrayList<Composition> foundCompositions = this.compositions
                        .stream()
                        .filter(p-> p.getTitle().toLowerCase().contains(searchString.toLowerCase()))
                        .collect(Collectors.toCollection(ArrayList<Composition>::new));
                
                if (foundCompositions.size() == 0) {
                    System.out.println("Композицій, назва яких містить " + searchString + ", не знайдено.\n");
                } else {
                    System.out.println("Композиції, назва яких містить \"" + searchString + "\":\n");
                    this.printCompositions(foundCompositions);
                }
                
                this.mode = MenuMode.MAIN;
                this.addCount = 0;
            }
        } else { // Верхній рівень меню
            if  (scanner.hasNextInt()) {
                int choice = scanner.nextInt();
                scanner.nextLine();
                switch (choice) {
                case 1:
                    // Виведемо всі композиції на екран
                    System.out.println("Зараз в нашій аудіотеці зберігаются наступні композиції:\n");
                    this.printCompositions(this.compositions);
                    break;
                case 2:
                    // Додамо композицію шляхом введення її назви, назви альбому т аімені виконавця
                    this.mode = MenuMode.ADD_ELEMENT;
                    break;
                case 3:
                    // Видалимо останню композицію
                    this.compositions.remove(this.compositions.size() - 1);
                    System.out.println("Після видалення композиції в нашій аудіотеці зберігаются наступні композиції:\n");
                    this.printCompositions(this.compositions);
                    break;
                case 4:
                    // Шукатимемо композицію за назвою
                    this.mode = MenuMode.SEARCH_ELEMENTS;
                    break;
                case 5:
                    // Відсортуємо композиції за назвою
                    this.compositions.sort(Comparator.comparing(Composition::getTitle));
                    System.out.println("Після сортування композиції в нашій аудіотеці розташовані в такому порядку:\n");
                    this.printCompositions(this.compositions);
                    break;
                case 6:
                    // Запишемо композиції у файл
                    this.saveCompositions(this.compositions, "File1.txt");
                    break;
                case 7:
                    // Вийдемо з програми
                    scanner.close();
                    this.exit();
                    break;
                default:
                    System.out.println(Menu.MSG_INVALID_CHOICE);
                    break;
                }
            } else {
                System.out.println(Menu.MSG_INVALID_CHOICE);
            }
        }
        
        this.display();
        scanner.nextLine();
    }
    
    /**
     * Exits from the application.
     */
    private void exit() {
        System.out.println("Виходимо з програми...");
        System.exit(1); 
    }
    
    /**
     * Outputs compositions to console.
     * @param compositionsToPrint compositions to output to console
     */
    private void printCompositions(ArrayList<Composition> compositionsToPrint) {
        Iterator<Composition> iterator = compositionsToPrint.iterator();
        while (iterator.hasNext()) {
            this.printComposition(iterator.next());
        }
    }
    
    /**
     * Outputs a single composition to console.
     * @param composition composition to output to console
     */
    private void printComposition(Composition composition) {
        System.out.println(composition);
        System.out.println("");
    }
    
    /**
     * Outputs compositions to file.
     * @param compositions compositions to save to file
     * @param fileName file name of file to save compositions to
     */
    private void saveCompositions(ArrayList<Composition> compositions, String fileName) {
        try {
            FileOutputStream fos = new FileOutputStream(fileName);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(compositions.toString());
            oos.close();
            fos.close();
            System.out.println("Композиції у вигляді текстового рядка успішно записано в файл\"" + fileName + "\".\n");
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
