package ua.khpi.oop.pudrovska08;

/**
 * Represents audio format of a composition.
 */
public enum AudioFormat {
	/**
     * Unknown format
     */
    UNKNOWN("Невідомий"),
    /**
     * MP3 format
     */
    MP3("MP3"),
    /**
     * WAV format
     */
    WAV("WAV"),
    /**
     * AIFF format
     */
    AIFF("AIFF"),
    /**
     * FLAC format
     */
    FLAC("FLAC"),
    /**
     * AAC format
     */
    AAC("AAC");

    private final String name;

    private AudioFormat(String s) {
        name = s;
    }

    /**
     * Compares the current value to a string.
     * @param otherName string to compare the current value to
     * @return true if the current value is equal to the other string, false if not
     */
    public boolean equalsName(String otherName) {
        return (otherName == null) ? false : name.equals(otherName);
    }

    /**
     * Converts the current value to string.
     * @return string representation of the current value
     */
    public String toString() {
        return this.name;
    }
}
