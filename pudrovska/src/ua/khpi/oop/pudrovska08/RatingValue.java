package ua.khpi.oop.pudrovska08;

/**
 * Represents user's rating (opinion) of a composition.
 */
public enum RatingValue {
	/**
     * The user considers the composition terrible.
     */
    TERRIBLE,
    /**
     * The user considers the composition bad.
     */
    BAD,
    /**
     * The user considers the composition so-so.
     */
    SO_SO,
    /**
     * The user considers the composition good.
     */
    GOOD,
    /**
     * The user considers the composition excellent.
     */
    EXCELLENT
}