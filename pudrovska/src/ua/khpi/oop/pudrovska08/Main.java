package ua.khpi.oop.pudrovska08;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.UUID;

/**
 * The program implements a console application that
 * allows users to work with compositions
 * (add, remove, search, and save them).
 * @author  Olga Pudrovska
 * @version 1.0
 * @since   2016-10-15 
 */
public class Main {

	/**
	 * This is the main method which makes use of Composition class and RatingValue enumeration.
	 * @param args command line parameters
	 */
    public static void main(String[] args) {
        LocalDate currentLocalDate = Instant.ofEpochMilli(System.currentTimeMillis()).atZone(ZoneId.systemDefault()).toLocalDate();
        
        TreeMap<String, RatingValue> ratings = new TreeMap<String, RatingValue>();
        
        // Створимо першу тестову композицію
        ratings.put("Слухач1", RatingValue.EXCELLENT);
        ratings.put("Слухач2", RatingValue.GOOD);
        ratings.put("Слухач3", RatingValue.EXCELLENT);
        ratings.put("Слухач4", RatingValue.SO_SO);
        ratings.put("Слухач5", RatingValue.GOOD);
        
        Composition composition1 = new Composition(UUID.randomUUID().toString(), "ВВВ Назва 1", "Альбом 1", "Виконавець 1", "Рок",
                "Текст пісні 1", currentLocalDate, currentLocalDate, 600000, AudioFormat.MP3, ratings);
        
        // Створимо другу тестову композицію
        ratings.clear();
        ratings.put("Слухач1", RatingValue.EXCELLENT);
        ratings.put("Слухач2", RatingValue.BAD);
        ratings.put("Слухач3", RatingValue.SO_SO);
        
        Composition composition2 = new Composition(UUID.randomUUID().toString(), "ААА Назва 2", "Альбом 2", "Виконавець 2", "Поп",
                "Текст пісні 2", currentLocalDate, currentLocalDate, 300000, AudioFormat.FLAC, ratings);
        
        // Створимо третю тестову композицію
        ratings.clear();
        ratings.put("Слухач1", RatingValue.EXCELLENT);
        ratings.put("Слухач2", RatingValue.EXCELLENT);
        ratings.put("Слухач3", RatingValue.GOOD);
        
        Composition composition3 = new Composition(UUID.randomUUID().toString(), "БББ Назва 3", "Альбом 3", "Виконавець 3", "Класика",
                "", currentLocalDate, currentLocalDate, 1000000, AudioFormat.WAV, ratings);
        
        ArrayList<Composition> compositions = new ArrayList<Composition>();
        
        compositions.add(composition1);
        compositions.add(composition2);
        compositions.add(composition3);
        
        Menu menu = new Menu(compositions);
        menu.display();
    }
}
